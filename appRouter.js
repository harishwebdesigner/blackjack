import React from 'react';
import {Stack, Router, Scene} from 'react-native-router-flux';
import { Provider, connect } from 'react-redux';
import SplashScreen from './src/pages/splashScreen';
import GameTable from './src/pages/gameTable';
import configureStore from './appStore';
// import {ROUTE_NAMES} from './constants';

const store = configureStore();
const RouterWithRedux = connect()(Router);
// const routeName = ROUTE_NAMES.APP;

const Routes = () => (
    <Provider store={store}>
        <RouterWithRedux>
            <Stack key="root">
                <Scene key="splashScreen" component={SplashScreen} title="BlackJack" initial={true} hideNavBar/>
                <Scene key="gameTable" drawer={true} component={GameTable} title="Blackjack " hideNavBar/>
            </Stack>
        </RouterWithRedux>
    </Provider>
)

export default Routes;