export const CARD_NUMBERS = {
  'A': {
    value: 11,
    label: 'A'
  },
  '2': {
    value: 2,
    label: '2'
  },
  '3': {
    value: 3,
    label: '3'
  },
  '4': {
    value: 4,
    label: '4'
  },
  '5': {
    value: 5,
    label: '5'
  },
  '6': {
    value: 6,
    label: '6'
  },
  '7': {
    value: 7,
    label: '7'
  },
  '8': {
    value: 8,
    label: '8'
  },
  '9': {
    value: 9,
    label: '9'
  },
  '10': {
    value: 10,
    label: '10'
  },
  'J': {
    value: 10,
    label: 'J'
  },
  'Q': {
    value: 10,
    label: 'Q'
  },
  'K': {
    value: 10,
    label: 'K'
  },
};

export const CARD_SYMBOLS = {
  DIAMONDS: 'DIAMONDS',
  CLUBS: 'CLUBS',
  HEARTS: 'HEARTS',
  SPADES: 'SPADES'
};

export const PLAYER_STATUS = {
  BUST: 'BUST',
  STAND: 'STAND',
  SURRENDER: 'SURRENDER',
  BLACKJACK: 'BLACKJACK'
};

export const GAME_RESULTS = {
  DEALER_WON: 'DEALER_WON',
  PLAYER_WON: 'PLAYER_WON',
  PUSH: 'PUSH'
};

export const NUMBER_OF_DECKS_ON_TABLE = 3;

export const NUMBER_OF_CARDS_ON_START = 2;

export const DEFAULT_DEAL_AMOUNT = 40;

export const INITIAL_BALANCE_OF_PLAYER = 889900;