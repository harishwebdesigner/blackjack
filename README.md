# BlackJack

## Steps to setup BlackJack:

* Clone BlackJack repository.
* Open terminal and go to the path where BlackJack is present in your computer (Like: cd /Documents/projects/BlackJack).
* Run command: npm install
* Please connect a physical android device or use emulator to install app.
* Run command: react-native run-android --variant=release
