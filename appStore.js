import {createStore, applyMiddleware} from 'redux';
import reducer from './CombineReducer';
import {createLogger} from 'redux-logger';

const thunk = (store) => (next) => (action) => 
    typeof action === 'function' ?
        action(store.dispatch) :
        next(action);


const configureStore = (initialState={}) => {
    const middleware = [thunk];
    if(__DEV__) {
        middleware.push(createLogger());
    }
    return createStore(reducer, initialState, applyMiddleware(...middleware));
}

export default configureStore;
