import {combineReducers} from 'redux';
import RouterFluxReducer from './RouterReducer';

export default combineReducers({
    RouterFluxReducer
});