import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
} from 'react-native';
import Routes from './appRouter';

export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar hidden barStyle="dark-content" />
        <SafeAreaView style={{flex: 1}}>
          <Routes/>
        </SafeAreaView>
      </View>
    );
  }
}