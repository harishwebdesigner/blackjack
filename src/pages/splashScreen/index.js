
import React from 'react';
import {
  ScrollView,
  View,
  Text,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from './splashScreenCss';
import PageBackground from '../../../assets/splashScreenBg.jpg';


class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    onPlay = () => {
      Actions.gameTable();
    }

    render() {
      return (
        <ImageBackground source={PageBackground} style={styles.pageWrapper} imageStyle={styles.pageBg}>
          <View style={styles.bgOverlay} />
          <TouchableOpacity style={styles.playBtn} activeOpacity={0.8} onPress={this.onPlay}>
            <Text style={styles.playBtnText}>PLAY GAME</Text>
          </TouchableOpacity>
        </ImageBackground>
      );
    }
}

export default SplashScreen;