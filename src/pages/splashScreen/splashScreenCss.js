
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = {
  pageWrapper: {
    flex: 1,
    background: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bgOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.6)'
  },
  playBtn: {
    backgroundColor: 'green',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 40,
    elevation: 10
  },
  playBtnText: {
    fontSize: 30,
    color: '#ffffff',
    fontWeight: 'bold',
    letterSpacing: 1
  }

};

export default styles;