
import React from 'react';
import {
  ScrollView,
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity
} from 'react-native';
import _ from 'lodash';
import styles from './gameTableCss';
import PageBackground from '../../../assets/table.jpg';
import ShoeDeck from '../../../assets/shoeDeck.png';
import Balance from './balance';
import DealMoneyCounter from './dealMoneyCounter';
import CardsInHand from './cardsInHand';
import {
  NUMBER_OF_DECKS_ON_TABLE,
  CARD_SYMBOLS,
  CARD_NUMBERS,
  NUMBER_OF_CARDS_ON_START,
  DEFAULT_DEAL_AMOUNT,
  INITIAL_BALANCE_OF_PLAYER,
  PLAYER_STATUS,
  GAME_RESULTS
} from '../../../constants';

class GameTable extends React.Component {
    constructor(props) {
        super(props);
        this.shoeCards = [];
        this.isDealOn = false;
        this.state = {
          ...this.getInitialStates(),
          balance: INITIAL_BALANCE_OF_PLAYER
        }
    }

    getInitialStates = () => {
      return {
        dealerCards: [],
        playerCards: [],
        dealerCardsTotal: 0,
        playerCardsTotal: 0,
        isDoubleBtnEnabled: true,
        dealAmount: DEFAULT_DEAL_AMOUNT,
        isDealerTurn: false,
        gameResult: ''
      };
    }

    getInHandCardsData = (cards) => {
      if (cards && cards.length) {
        let cardsTotal = 0;
        let updatedCards = _.cloneDeep(cards);
        cardsTotal = this.calculateCardsTotal(updatedCards);
        if (cardsTotal > 21) {
          cardsTotal = this.aceHandler(cardsTotal, cards).total;
          updatedCards = this.aceHandler(cardsTotal, cards).cards;
        }
        return {total: cardsTotal, cards: updatedCards};
      }
      return {};
    }

    calculateCardsTotal = (cards) => {
      let total = 0;
      if (cards && cards.length) {
        _.forEach(cards, function(cardData) {
          total = total + cardData.value;
        });
      }
      return total;
    }

    aceHandler = (total, cards) => {
      let newTotal = total;
      let updatedCards = _.cloneDeep(cards);
      _.forEach(updatedCards, function(cardData, key) {
        if (newTotal > 21) {
          if (cardData.value === 11 && cardData.label === 'A') {
            updatedCards[key].value = 1;
            newTotal = newTotal - 10;
          }
        }
      });
      return {total: newTotal, cards: updatedCards};
    }

    onStartNewDeal = () => {
      const {balance, dealAmount} = this.state;
      if (balance && balance >= dealAmount) {
        this.isDealOn = true;
        this.setState({
          balance: balance - dealAmount,
          gameResult: ''
        },() => {
          this.distributeInitialCards();
        });
      }
    }

    doubleBtnHandler = () => {
      const {balance, dealAmount} = this.state;
      if (balance >= dealAmount) {
        this.setState({
          balance: balance - dealAmount,
          dealAmount: dealAmount * 2
        }, () => {
          this.onDrawNewCard(true, true);
        });
      }
    }

    distributeInitialCards = () => {
      let isHitByPlayer = false;
      const dealerCards = [];
      const playerCards = [];
      for (let i = 0; i < NUMBER_OF_CARDS_ON_START * 2; i++) {
        isHitByPlayer = (i >= NUMBER_OF_CARDS_ON_START) ? true : false;
        if (isHitByPlayer) {
          playerCards.push(this.getNewCard());
        } else {
          dealerCards.push(this.getNewCard());
        }
      }
      this.setState({
        dealerCards, playerCards
      }, () => {
        this.setResultData(dealerCards);
        this.setResultData(playerCards, true);
      });
    }

    onDrawNewCard = (isHitByPlayer, isDoubleBet) => {
      const {dealerCards, playerCards} = this.state;
      const cards = _.cloneDeep(isHitByPlayer ? playerCards : dealerCards);
      cards.push(this.getNewCard());
      this.setResultData(cards, isHitByPlayer, isDoubleBet);
    }

    setResultData = (cards, isHitByPlayer, isDoubleBet) => {
      const updatedInHandCardsData = this.getInHandCardsData(cards);
      const {playerCardsTotal} = this.state;
      const {isDealOn} = this;
      if (isHitByPlayer) {
        this.setState({
          playerCardsTotal: updatedInHandCardsData.total || 0,
          playerCards: updatedInHandCardsData.cards || []
        }, () => {
          if (updatedInHandCardsData.total >= 21) {
            this.stopDrawCardsByPlayer();
            this.onPlayerBust();
          }
          if (isDoubleBet && updatedInHandCardsData.total <= 21) {
            this.onStandPress();
          }
        });
      } else {
        this.setState({
          dealerCardsTotal: updatedInHandCardsData.total || 0,
          dealerCards: updatedInHandCardsData.cards || []
        }, () => {
          if (!isDealOn) {
            if (!isDealOn && updatedInHandCardsData.total >= 17) {
              this.onEndGame();
            } else {
              this.onStandPress();
            }
          }
        });
      }
    }

    checkIfCardAlreadyOnTable = (cardsOnTable, cardSymbol, cardLabel) => {
      let isCardAlreadyOnTable = false;
      if (cardsOnTable && cardsOnTable.length && cardSymbol && cardLabel) {
        for (let i = 0; i < cardsOnTable.length; i++) {
          if (cardsOnTable[i].symbol === cardSymbol && cardsOnTable[i].label === cardLabel) {
            isCardAlreadyOnTable = true;
            break;
          }
        }
      }
      return isCardAlreadyOnTable;
    }

    getNewCard = () => {
      const {dealerCards, playerCards} = this.state;
      const totalCardSymbols = Object.keys(CARD_SYMBOLS);
      const totalCardLabels = Object.keys(CARD_NUMBERS);
      const randomCardSymbol = CARD_SYMBOLS[totalCardSymbols[Math.floor((Math.random() * totalCardSymbols.length))]];
      const randomCardLabel = CARD_NUMBERS[totalCardLabels[Math.floor((Math.random() * totalCardLabels.length))]].label;
      const cardsOnTable = {...dealerCards, ...playerCards};
      if (this.checkIfCardAlreadyOnTable(cardsOnTable, randomCardSymbol, randomCardLabel)){
        this.getNewCard();
      } else {
        return {
          ...CARD_NUMBERS[randomCardLabel],
          symbol: randomCardSymbol
        }
      }
    }
    
    setDealAmount = (dealAmount) => {
      this.setState({dealAmount});
    }

    onHitPress = () => {
      const {playerCardsTotal} = this.state;
      if (playerCardsTotal <= 21) {
        this.onDrawNewCard(true);
      }
    }

    onEndGame = () => {
      const {dealAmount, balance} = this.state;
      const gameResult = this.getGameResult();
      let updatedBalance = balance;
      if (gameResult === GAME_RESULTS.PLAYER_WON) {
        updatedBalance = updatedBalance + (dealAmount * 2);
      } else if (gameResult === GAME_RESULTS.PUSH) {
        updatedBalance = updatedBalance + dealAmount;
      }
      this.setState({
        isDealerTurn: false,
        gameResult,
        balance: updatedBalance
      });
    }

    getGameResult = () => {
      const {dealerCardsTotal, playerCardsTotal} = this.state;
      let result = '';
      if (playerCardsTotal > 21) {
        result = GAME_RESULTS.DEALER_WON;
      } else if (dealerCardsTotal > 21) {
        result = GAME_RESULTS.PLAYER_WON;
      } else if (dealerCardsTotal > playerCardsTotal) {
        result = GAME_RESULTS.DEALER_WON;
      } else if (playerCardsTotal > dealerCardsTotal) {
        result = GAME_RESULTS.PLAYER_WON;
      } else {
        result = GAME_RESULTS.PUSH;
      }
      return result;
    }

    getResultBadge = (isDealer) => {
      const {gameResult} = this.state;
      switch (gameResult) {
        case GAME_RESULTS.DEALER_WON:
          return isDealer ? 'WON' : 'LOSE';
        case GAME_RESULTS.PLAYER_WON:
          return isDealer ? 'LOSE' : 'WON';
        case GAME_RESULTS.PUSH:
            return 'PUSH';
        default:
          return '';
      }
    }

    playerBustHandler = () => {
      this.onEndGame();
    }

    standHandler = () => {
      const {dealerCardsTotal} = this.state;
      const {isDealOn} = this;
      this.stopDrawCardsByPlayer();
      if (dealerCardsTotal <= 16) {
        this.onDrawNewCard(false);
      } else {
        this.onEndGame();
      }
    }

    surrenderHandler = () => {
      const {balance, dealAmount} = this.state;
      this.isDealOn = false;
      this.setState({
        balance: balance + dealAmount / 2,
        isDoubleBtnEnabled: true,
        dealAmount: DEFAULT_DEAL_AMOUNT
      });
    }

    onPlayerBust = () => {
      this.onDealerTurn(PLAYER_STATUS.BUST);
    }
    
    onStandPress = () => {
      this.onDealerTurn(PLAYER_STATUS.STAND);
    }

    onSurrender = () => {
      this.onDealerTurn(PLAYER_STATUS.SURRENDER);
    }

    onPlayerBlackJack = () => {
      this.onDealerTurn(PLAYER_STATUS.BLACKJACK);
    }

    onDealerTurn = (playerStatus) => {
      switch (playerStatus) {
        case PLAYER_STATUS.BUST:
          this.playerBustHandler();
          break;
        case PLAYER_STATUS.STAND:
          this.standHandler();
          break;
        case PLAYER_STATUS.SURRENDER:
          this.surrenderHandler();
          break;
        case PLAYER_STATUS.BLACKJACK:
          this.standHandler();
          break;
        default:
          break;
      }
    }

    stopDrawCardsByPlayer = () => {
      this.isDealOn = false;
    }

    render() {
      const {
        dealerCards,
        playerCards,
        dealAmount,
        balance,
        dealerCardsTotal,
        playerCardsTotal,
        gameResult
      } = this.state;
      const {isDealOn} = this;
      const bottomContentStyle = !isDealOn ? styles.bottomContentDealOn : {};
      return (
        <ImageBackground source={PageBackground} style={styles.pageWrapper} imageStyle={styles.pageBg}>
          {/* <View style={styles.bgOverlay} /> */}
          <View style={styles.pageContent}>
            <Balance
              balance={balance}
            />
            <CardsInHand
              isDealer={true}
              cards={dealerCards}
              cardsTotal={dealerCardsTotal}
              isDealOn={isDealOn}
              badgeText={this.getResultBadge(true)}
            />
            <CardsInHand
              cards={playerCards}
              cardsTotal={playerCardsTotal}
              badgeText={this.getResultBadge(false)}
            />
            {!isDealOn ?
              <TouchableOpacity style={[styles.circularButton, styles.dealButton]} onPress={this.onStartNewDeal}>
                <Text style={styles.circularButtonText}>DEAL</Text>
              </TouchableOpacity> 
              : null
            }
              <View style={[styles.bottomContent, bottomContentStyle]} onPress={this.onStartNewDeal}>
                {isDealOn ?
                  <View style={styles.sideButtonsWrapper}>
                    <TouchableOpacity style={styles.circularButton} onPress={this.onSurrender}>
                      <Text style={[styles.circularButtonText, styles.surrenderButton]}>SURRENDER</Text>
                    </TouchableOpacity>
                    {balance >= dealAmount ?
                      <TouchableOpacity style={styles.circularButton} onPress={this.doubleBtnHandler}>
                        <Text style={styles.circularButtonText}>DOUBLE</Text>
                      </TouchableOpacity> : null
                    }
                  </View> : null
                }
                <DealMoneyCounter
                  isCounterEnabled={isDealOn}
                  onDealAmountUpdate={this.setDealAmount}
                  dealAmount={dealAmount}
                  balance={balance}
                  isDealOn={isDealOn}
                />
                {isDealOn ?
                  <View style={styles.sideButtonsWrapper}>
                    <TouchableOpacity style={styles.circularButton} onPress={this.onHitPress}>
                      <Text style={styles.circularButtonText}>HIT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.circularButton} onPress={this.onStandPress}>
                      <Text style={styles.circularButtonText}>STAND</Text>
                    </TouchableOpacity>
                  </View> : null
                }
              </View>
          </View>
          <Image source={ShoeDeck} style={styles.shoeDeck} />
        </ImageBackground>
      );
    }
}

export default GameTable;