
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = {
  counterWrapper: {
    width: 250,
    height: 40,
    borderRadius: 40,
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    justifyContent: 'space-between',
    elevation: 5
  },
  label: {
    fontSize: 24,
    fontWeight: 'bold',
    lineHeight: 30,
    color: '#000000'
  },
  counterButton: {
    width: 40,
    height: 40,
    backgroundColor: '#f00000',
    alignItems: 'center',
    justifyContent: 'center'
  },
  counterButtonText: {
    fontSize: 30,
    lineHeight: 35,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff'
  },
  counterButtonPositive: {
    backgroundColor: '#29a91a'
  }
};

export default styles;