
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './dealMoneyCounterCss';
import {DEFAULT_DEAL_AMOUNT} from '../../../../constants';

class Balance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          ...this.getInitialStates()
        }
    }

    getInitialStates = () => {
      return {
        value: this.props.dealAmount,
        isIncrementDisabled: false,
        isDecrementDisabled: false
      }
    }

    onChange = (isReduce) => {
      const {dealAmount, onDealAmountUpdate, balance} = this.props;
      let updatedValue = isReduce ? dealAmount - 10 : dealAmount + 10;
      let isDecrementDisabled = false;
      let isIncrementDisabled = false;
      if (updatedValue <= DEFAULT_DEAL_AMOUNT) {
        updatedValue = DEFAULT_DEAL_AMOUNT;
        isDecrementDisabled = true;
      }
      if (updatedValue >= balance) {
        isIncrementDisabled = true;
        updatedValue = balance;
      }
      if (balance <= DEFAULT_DEAL_AMOUNT) {
        isDecrementDisabled = true;
        isIncrementDisabled = true;
      }
      this.setState({
        isDecrementDisabled, isIncrementDisabled
      }, () => onDealAmountUpdate(updatedValue));
    }

    render() {
      const {dealAmount, isDealOn} = this.props;
      const isDecrementDisabled = isDealOn ? isDealOn : this.state.isDecrementDisabled;
      const isIncrementDisabled = isDealOn ? isDealOn : this.state.isIncrementDisabled;
      return (
        <View style={styles.counterWrapper}>
          <TouchableOpacity
            style={styles.counterButton}
            disabled={isDecrementDisabled}
            onPress={() => this.onChange(true)}
          >
            <Text style={styles.counterButtonText}>-</Text>
          </TouchableOpacity>
          <Text style={styles.label}>$ {dealAmount}</Text>
          <TouchableOpacity
            style={[styles.counterButton, styles.counterButtonPositive]}
            disabled={isIncrementDisabled}
            onPress={() => this.onChange(false)}
          >
            <Text style={styles.counterButtonText}>+</Text>
          </TouchableOpacity>
        </View>
      );
    }
}

export default Balance;