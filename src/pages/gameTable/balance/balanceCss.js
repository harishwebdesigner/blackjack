
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = {
  balanceWrapper: {
    borderColor: '#ffffff',
    borderWidth: 3,
    width: 250,
    height: 40,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#5a4434',
    paddingHorizontal: 15
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 20,
    color: '#ffffff'
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
    lineHeight: 28,
    color: '#f6c94c'
  }
};

export default styles;