
import React from 'react';
import {View, Text} from 'react-native';
import styles from './balanceCss';

class Balance extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
      const {balance} = this.props;
      return (
        <View style={styles.balanceWrapper}>
          <Text style={styles.label}>Balance: </Text>
          <Text style={styles.value}>$ {balance}</Text>
        </View>
      );
    }
}

export default Balance;