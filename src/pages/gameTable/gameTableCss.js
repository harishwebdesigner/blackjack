
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Dimensions} from 'react-native';

const styles = {
  pageWrapper: {
    flex: 1
  },
  pageContent: {
    padding: 10,
    flex: 1,
    position: 'relative',
    justifyContent: 'space-between'
  },
  bgOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.3)'
  },
  shoeDeck: {
    width: 100,
    height: 100,
    position: 'absolute',
    right: 140,
    top: 80
  },
  circularButton: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderColor: '#000000',
    borderWidth: 1,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3e7bb7'
  },
  circularButtonText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ffffff'
  },
  surrenderButton: {
    fontSize: 10
  },
  dealButton: {
    position: 'absolute',
    zIndex: 3,
    elevation: 7,
    top: Dimensions.get('window').height/2 - 35,
    left: Dimensions.get('window').width/4 - 35
  },
  bottomContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  bottomContentDealOn: {
    justifyContent: 'center'
  },
  sideButtonsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    flex: 1
  }
};

export default styles;