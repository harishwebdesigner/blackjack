
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = {
  dealerCardsWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 40,
    position: 'relative',
    alignSelf: 'center',
    overflow: 'visible'
  },
  cardTotalBadge: {
    position: 'absolute',
    zIndex: 2,
    top: -20,
    right: -20,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#f00000',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 6
  },
  cardTotalBadgeText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffffff',
    textAlign: 'center'
  },
  resultBadge: {
    position: 'absolute',
    zIndex: 1,
    top: 35,
    left: -80,
    width: 70,
    height: 25,
    borderRadius: 4,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 6
  },
  resultBadgeText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#f00000',
    textAlign: 'center'
  },
  resultBadgeTextWon: {
    color: 'green'
  },
};

export default styles;