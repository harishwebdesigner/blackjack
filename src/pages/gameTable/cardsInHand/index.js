
import React from 'react';
import {View, Text} from 'react-native';
import _ from 'lodash';
import styles from './cardsInHandCss';
import Card from '../../../components/card';

class CardsInHand extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
      const {cards, cardsTotal, isDealer, isDealOn, badgeText} = this.props;
      if (cards && cards.length) {
        const badgeTextWonStyle = badgeText === 'WON' ? styles.resultBadgeTextWon : {};
        return (
          <View style={styles.dealerCardsWrapper}>
            {
              _.map(cards, (cardData, key) => {
                let isHiddenCard;
                if (key === 1 && isDealer && isDealOn) {
                  isHiddenCard = true;
                }
                return (
                  <Card
                    cardSymbol={cardData.symbol}
                    cardLabel={cardData.label}
                    isHiddenCard={isHiddenCard}
                  />
                );
              })
            }
            {badgeText && badgeText.length ?
              <View style={styles.resultBadge}>
                <Text style={[styles.resultBadgeText, badgeTextWonStyle]}>{badgeText}</Text>
              </View>: null
            }
            {(isDealer && !isDealOn) || !isDealer ?
              <View style={styles.cardTotalBadge}>
                <Text style={styles.cardTotalBadgeText}>{cardsTotal}</Text>
              </View> : null
            }
          </View>
        );
      }
      return null;
    }
}

export default CardsInHand;