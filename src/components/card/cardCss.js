
const styles = {
  card: {
    width: 70,
    height: 90,
    backgroundColor: '#ffffff',
    paddingHorizontal: 4,
    borderRadius: 4,
    elevation: 5,
    borderColor: '#9a9a9a',
    borderWidth: 1,
    marginLeft: -40,
    zIndex: 1,
  },
  cardInner: {
    flex: 1
  },
  firstCard: {
    marginLeft: 0
  },
  label: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  cardSymbolSmall: {
    width: 20,
    height: 20,
    marginLeft: -3
  },
  cardSymbol: {
    position: 'absolute',
    left: 15,
    bottom: 5,
    width: 50,
    height: 50
  },
  hiddenCardImage: {
    height: '90%',
    width: null,
    marginTop: 4
  }
};

export default styles;