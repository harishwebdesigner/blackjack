
import React from 'react';
import {View, Text, Image} from 'react-native';
import PropTypes from 'prop-types';
import styles from './cardCss';
import {CARD_NUMBERS, CARD_SYMBOLS} from '../../../constants';
import ClubsIcon from '../../../assets/cardSymbols/clubs.png';
import DiamondsIcon from '../../../assets/cardSymbols/diamonds.png';
import HeartsIcon from '../../../assets/cardSymbols/hearts.png';
import SpadesIcon from '../../../assets/cardSymbols/spades.png';
import HiddenCard from '../../../assets/hiddenCard.jpg';

const CARD_IMAGES = {
  DIAMONDS: DiamondsIcon,
  CLUBS: ClubsIcon,
  HEARTS: HeartsIcon,
  SPADES: SpadesIcon 
}

class Card extends React.Component {
  constructor(props) {
      super(props);
  }
  render () {
    const  {cardSymbol, cardLabel, isFirstCard, isHiddenCard} = this.props;
    const firstCardStyle = isFirstCard ? styles.firstCard : {};
    if (
      cardSymbol &&
      cardSymbol.length &&
      cardLabel &&
      cardLabel.length
    ) {
      return (
        <View style={[styles.card, firstCardStyle]}>
          {!isHiddenCard ?
            <View style={styles.cardInner}>
              <Text style={styles.label}>{CARD_NUMBERS[cardLabel].label}</Text>
              <Image source={CARD_IMAGES[cardSymbol]} style={styles.cardSymbolSmall} />
              <Image source={CARD_IMAGES[cardSymbol]} style={styles.cardSymbol} />
            </View> : 
            <Image source={HiddenCard} style={styles.hiddenCardImage} />
          }
        </View>
      );
    } return null;
  }
}

Card.propTypes = {
  cardLabel: PropTypes.string.isRequired,
  cardSymbol: PropTypes.string.isRequired,
  isFirstCard: PropTypes.bool,
  isHiddenCard: PropTypes.bool
};

Card.defaultProps = {
  isFirstCard: false,
  isHiddenCard: false
};

export default Card;